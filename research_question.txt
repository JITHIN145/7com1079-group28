Group: group 28

Question
========
RQ: Is there any correlation between ball possession and Attempts on goal ?

Null hypothesis: 'There is no correlation between ball possession and Attempts on goal'.

Alternative hypothesis: 'There is a correlation between ball possession and Attempts on goal'.

Dataset
=======

URL: https://www.kaggle.com/mathan/fifa-2018-match-statistics?select=FIFA+2018+Statistics.csv

Column Headings:

```
> FIFA_2018_Statistics <- read.csv("C:/Users/KANNAN/Desktop/Folder/FIFA 2018 Statistics.csv")
> colnames(FIFA_2018_Statistics)
 [1] "Date"                   "Team"                   "Opponent"              
 [4] "Goal.Scored"            "Ball.Possession.."      "Attempts"              
 [7] "On.Target"              "Off.Target"             "Blocked"               
[10] "Corners"                "Offsides"               "Free.Kicks"            
[13] "Saves"                  "Pass.Accuracy.."        "Passes"                
[16] "Distance.Covered..Kms." "Fouls.Committed"        "Yellow.Card"           
[19] "Yellow...Red"           "Red"                    "Man.of.the.Match"      
[22] "X1st.Goal"              "Round"                  "PSO"                   
[25] "Goals.in.PSO"           "Own.goals"              "Own.goal.Time" 
```